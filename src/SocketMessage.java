/**
 * SocketMessage offers an incredibly simplistic approach to avoiding writing a
 * protocol, always sending a fixed number of bytes across the provided socket, 
 * and always expecting to receive that amount of bytes.
 * @author Ryan Leonard 
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
	
public class SocketMessage{

	/**
	 * Sends a fixed length message to destination at sock.
	 * @param sock Socket representation of the destination at which to send the message. (Assumed to be connected)
	 * @param message The message to be sent, cannot be larger than MAX_MESSAGE bytes.
	 * @throws IOException If message exceeds MAX_MESSAGE, or if there is
	 * general connectivity issues with sock.
	 */
	public static void sendMessage(Socket sock, String message) throws IOException{
		byte[] buffer = new byte[MAX_MESSAGE],
				tmp_buf;
		OutputStream out = sock.getOutputStream();
		
		if (message.length() >= MAX_MESSAGE )
			throw new IOException("Unable to send message, too long for MAX_MESSAGE: " + MAX_MESSAGE);
		tmp_buf = message.getBytes();
		for (int i = 0 ; i < tmp_buf.length ; i++)
			buffer[i] = tmp_buf[i];
		
		out.write(buffer, 0, MAX_MESSAGE);
	}
	
	/**
	 * Receives a fixed length message from sender at sock.
	 * @param sock Socket representation of the sender. (Assumed to be connected)
	 * @return Message received from sender parsed and placed into system character set String.
	 * @throws IOException If there is general connectivity issues with sock.
	 */
	public static String recvMessage(Socket sock) throws IOException{
		byte[] buffer = new byte[MAX_MESSAGE];
		InputStream in = sock.getInputStream();
		String val = new String();
		int ret;
		
		for(int i = 0 ; i < MAX_MESSAGE ; ){
			ret = in.read(buffer, i, MAX_MESSAGE-i);
			if (ret == -1)
				throw new IOException("Received end of transmission while awaiting message.");
			else
				i+=ret;
		}
		for(int i=0 ; buffer[i] != 0 ; i++)
			val += (char)buffer[i];
		return val;
	}

	/**
	 * The maximum number of bytes that can be transmitted using this class.
	 */
	public final static Integer
		MAX_MESSAGE = 1024;
}